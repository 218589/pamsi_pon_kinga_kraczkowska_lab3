#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include<assert.h>

using namespace std;
const int initStack = 1;

class IStack
{
public:
      IStack ();                 // utwórz pusty stos
      ~IStack ();
      void Push ( int i );       // dodaj liczbę na szczyt stosu
      int  Pop ();               // zdejmij liczbę ze szczytu stosu
      int  Top () const;         // co jest na szczycie stosu?
      bool IsEmpty () const;     // czy stos jest pusty?
	 void wypisz();
private:
      void Grow ();
      int  *_arr;      // adres pierwszego elementu tablicy
      int   _capacity; // bieżący rozmiar tablicy
      int   _top;      // ilość elementów na stosie
};

IStack::IStack ()
      : _top (0), _capacity (initStack)
{
      _arr = new int [initStack]; // utwórz tablicę (przydziel pamięć)
}

IStack::~IStack ()
{
      delete []_arr; // usuń tablicę z pamięci komputera (zwolnij pamięć)
}

void IStack::Push (int i)
{
      assert (_top <= _capacity);
      if (_top == _capacity)
            Grow ();
      _arr [_top] = i;
      ++_top;
}

void IStack::Grow ()
{

      //cout << "Podwajanie stosu o rozmiarze: " << _capacity << ".\n";
      // utwórz  nową tablicę
      int * arrNew = new int [2* _capacity];
      // skopiuj wszystkie elementy
      for (int i = 0; i < _capacity; ++i)
            arrNew [i] = _arr [i];
      _capacity = 2* _capacity;
      // zwolnij pamięć, zajmowaną przez starą tablicę
      delete []_arr;
      // zapisz adres nowej tablicy w składowej _arr
      _arr = arrNew;
}

int IStack::Pop ()
{// Nie wolno pobierać liczb z pustego stosu
      assert (_top > 0);
      --_top;
      return _arr [_top];
}

int IStack::Top () const
{
      // Metodę Top wolno wywoływać tylko wtedy, gdy stos nie jest pusty
      assert (_top > 0);
      return _arr [_top - 1];
}

bool IStack::IsEmpty () const
{
      assert (_top >= 0); // sprawdzamy, czy zmienna _top spełnia kontrakt stosu
      return _top == 0;
}

void IStack::wypisz()
{
        int i;
        assert (_top <= _capacity);
	for(i=1;i<_top;i++)
		cout<<_arr[i]<<"\t";
	cout<<"\n";
}

int main ()
{

int tab[]={1000, 10000, 100000 ,500000.};
double start, stop;
int j=0;
int i=0;
for(i; i<4;++i)
{ IStack stos;
start = stop= (double)clock();
while(j < tab[i])
{
stos.Push(rand()%50);
j++;
}
stop = (double)clock();
cout << (stop-start) / CLOCKS_PER_SEC * 1000 << " ms." << endl;
}
return 0;
}
