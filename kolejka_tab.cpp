#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include<assert.h>

using namespace std;
const int initKol= 1;

class IKol
{
public:
      IKol ();                 // konstruktor
      ~IKol ();
      void Push ( int i );       // dodaj liczbę na szczyt stosu
      int  Pop ();               // usun liczbę ze szczytu stosu
      int  Top () const;         // podejrzyj szczyt
      bool IsEmpty () const;     // czy stos jest pusty
      void wypisz();
      void usun();
//private:
      void Grow ();
      int f;   //poczatek
      int l;   //koniec kolejki
      int * _arr;      // adres pierwszego elementu tablicy
      int   _capacity; // bieżący rozmiar tablicy
      int   _top;      // ilość elementów w kolejce
};

IKol::IKol ()
      : _top (0), _capacity (initKol)
{
      _arr = new int [initKol]; // utwórz tablicę (przydziel pamięć)
        f=0;
        l=0;
}

IKol::~IKol ()
{
      delete []_arr; // usuń tablicę z pamięci komputera (zwolnij pamięć)
}

void IKol::Push (int i)
{
      assert (_top <= _capacity);
      if (_top == _capacity)
            Grow ();
      _arr [_top] = i;
      ++_top;
      l++;
}

void IKol::Grow ()
{

      //cout << "Podwajanie kolejki o rozmiarze: " << _capacity << ".\n";
      // utwórz e nową tablicę
      int * arrNew = new int [1+ _capacity];
      // skopiuj wszystkie elementy
      for (int i = 0; i < _capacity; ++i)
            arrNew [i] = _arr [i];
      _capacity = 1+ _capacity;
      // zwolnij pamięć, zajmowaną przez starą tablicę
      delete []_arr;
      // zapisz adres nowej tablicy w składowej _arr
      _arr = arrNew;
}



int IKol::Pop()
{ int temp =_arr[(f)%_capacity];
f++;
_top--;
return temp;

}




int IKol::Top () const
{
      // Metodę Top wolno wywoływać tylko wtedy, gdy stos nie jest pusty
      assert (_top > 0);
      return _arr [_top - 1];
}

bool IKol::IsEmpty () const
{
      assert (_top >= 0); // sprawdzamy, czy nie pusto
      return _top == 0;
}



void IKol::wypisz()
{
        int i;
        assert (_top <= _capacity);
	for(int i = f % _capacity; i < f % _capacity + _top; i++)
		cout<<_arr[i]<<"\t";

}

void IKol::usun()
{ while(!IsEmpty())
 Pop();

}



int main ()
{

int tab[]={1000, 10000, 100000 ,500000.};
double start, stop;
int j=0;
int i=0;
for(i; i<4;++i)
{ IKol stos;
start = stop= (double)clock();
while(j < tab[i])
{
stos.Push(rand()%50);
j++;
}
stop = (double)clock();
cout << (stop-start) / CLOCKS_PER_SEC * 1000 << " ms." << endl;
}
return 0;
}
