#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include<assert.h>

using namespace std;
const int initStack = 1;

class IStack
{
public:
      IStack ();
      IStack (int);                 // utwórz pusty stos
      ~IStack ();
      void push ( int i );       // dodaj liczbę na szczyt stosu
      int  pop ();               // zdejmij liczbę ze szczytu stosu
      int  Top () const;         // co jest na szczycie stosu?
      bool IsEmpty () const;     // czy stos jest pusty?
	 void display();
private:
      void Grow ();
      int  *_arr;      // adres pierwszego elementu tablicy
      int   _capacity; // bieżący rozmiar tablicy
      int   _top;      // ilość elementów na stosie
};

IStack::IStack ( )
      : _top (0), _capacity (initStack)
{
      _arr = new int [initStack]; // utwórz tablicę (przydziel pamięć) na stercie
}

IStack :: IStack( int size )
{
    _top = -1;
    _capacity= size;

    if( size == 0 )
    {
        _arr = 0;
    }
    else
    {
        _arr = new int[ _capacity ];
    }
}


IStack::~IStack ()
{
      delete []_arr; // usuń tablicę z pamięci komputera (zwolnij pamięć)
}



void IStack :: push( int elem )
{
    if( _top == ( _capacity ) )
    {
        cout << "\nNie można dodac " << elem << ", full" << endl ;

            Grow ();
        return;
    }
    else
    {
        _top++;
        _arr[ _top ] = elem;
    }
}

void IStack::Grow ()
{

     // cout << "Podwajanie stosu o rozmiarze: " << _capacity << ".\n";
      // utwórz na stercie nową tablicę
      int * arrNew = new int [2 * _capacity];
      // skopiuj wszystkie elementy
      for (int i = 0; i < _capacity; ++i)
            arrNew [i] = _arr [i];
      _capacity = 2 * _capacity;
      // zwolnij pamięć, zajmowaną przez starą tablicę
      delete []_arr;
      // zapisz adres nowej tablicy w składowej _arr
      _arr = arrNew;
}



int IStack :: pop()
{
    if(IsEmpty () )
    {
        cout << "\n empty\n" ;

        return -1;
    }
    else
    {
        int ret = _arr[ _top ];
        _top--;

        return ret;
    }
}



int IStack::Top () const
{
      // Metodę Top wolno wywoływać tylko wtedy, gdy stos nie jest pusty
      assert (_top > 0);
      return _arr [_top - 1];
}



bool IStack::IsEmpty () const
{     if( _arr == 0 || _top == -1 )
      return true;
      else
        return false;
}


void IStack :: display()
{
    cout << "\n" ;
    for( int i = 0; i <= _top; i++ )
    {
        cout << _arr[ i ] << " " ;
    }
    if(IsEmpty () )
    {
        cout << "\nempty\n" ;
    }
    cout << endl ;
}



void hanoi(int number, IStack &stack1, IStack &stack2, IStack &stack3)
{
    int temp;

    if(number == 1)
    {
        temp = stack1.pop();
        stack3.push(temp);
    }
    else if(number == 2)
    {
        temp = stack1.pop();
        stack2.push(temp);
        temp = stack1.pop();
        stack3.push(temp);
        temp = stack2.pop();
        stack3.push(temp);
    }
    else if(number == 3)
    {
        temp = stack1.pop();
        stack3.push(temp);
        temp = stack1.pop();
        stack2.push(temp);
        temp = stack3.pop();
        stack2.push(temp);
        temp = stack1.pop();
        stack3.push(temp);
        temp = stack2.pop();
        stack1.push(temp);
        temp = stack2.pop();
        stack3.push(temp);
        temp = stack1.pop();
        stack3.push(temp);
    }
    else
    {
        hanoi(number - 1, stack1, stack3, stack2);

        temp = stack1.pop();
        stack3.push(temp);

        hanoi(number - 1, stack2, stack1, stack3);
    }

    cout << "\n----------------------------\n" ;

    stack1.display();
    stack2.display();
    stack3.display();

    cout << "\n----------------------------\n" ;
}

int main()
{
    int  num, ele;


    cout << "Podaj wysokość\n\n" ;
    cin >> num ;

    IStack stack1(num + 1);
    IStack stack2(num + 1);
    IStack stack3(num + 1);

    cout << "\nDodaj krazki (int):\n\n" ;
    for(int i = 0; i < num; i++)
    {
        cout << "Dodaj" << i+1 << " element\n" ;
        cin >> ele ;
        stack1.push(ele);
    }

    cout << "\n--------stworzono---------\n" ;

    stack1.display();
    stack2.display();
    stack3.display();

    cout << "\n----**********-------\n" ;

    hanoi(num, stack1, stack2, stack3);

    return 0;
}
